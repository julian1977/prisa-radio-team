// Load API & insert
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// Activar API YouTube
for (var e = document.getElementsByTagName("iframe"), x = e.length; x--;)
  if (/youtube.com\/embed/.test(e[x].src))
    if(e[x].src.indexOf('enablejsapi=') === -1)
      e[x].src += (e[x].src.indexOf('?') ===-1 ? '?':'&') + 'enablejsapi=1';


var player;

function onYouTubeIframeAPIReady(){

  player = new YT.Player('single-player', {
    /*events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }*/
    events: { 'onStateChange': onPlayerStateChange }
  });

}



function onPlayerStateChange(event){

  var current_time = Math.round(player.getCurrentTime());

  var videoData = player.getVideoData();
//videoData['title'];
//videoData['video_id'];
//videoData['author'];

  if(event.data == YT.PlayerState.PLAYING && current_time==0){
    s.usePlugins = false;
    s=s_gi(s.accountF)
    s.linkTrackVars='events,eVar2,eVar3,eVar4,eVar5,eVar8,eVar17,eVar18,eVar19,eVar20,eVar21,eVar22,eVar30,eVar32,eVar37,eVar39,eVar42,eVar43,eVar48,eVar60,eVar66,eVar70,eVar74';
    s.linkTrackEvents='event11,event12,event13,event14,event79';
    s.events="event11";
    s.eVar2='youtube';															 											// player
    s.eVar3=document.domain.replace(/www./gi,"") + location.pathname;		// pageName
    s.eVar4=section.toLowerCase();																				// Section
    if(subsection != '' ){s.eVar5 = section + '>' + subsection;}					// subsection
    s.eVar8=videoData['title'].toLowerCase();														// Name Video
    s.eVar17='web';																											// Canal
    s.eVar18='prisa';																										// Organization
    s.eVar19='radioacktiva';																							// Product
    s.eVar20=document.domain.replace(/www./gi,"");												// Domain|Subdomain
    s.eVar21=s.getNewRepeat().toLowerCase(); 														// User New / Repeat
    s.eVar22=status;																											// Logueado / Anonymous
    s.eVar30='radio';   										  						    						// Business Unit
    s.eVar32=s.getVisitNum();																						// Visit Number By Month
    s.eVar39=document.title.toLowerCase();																// Title
    s.eVar42='vod';														      										// Reproduction (vod,streaming)
    s.eVar43=userId; 																										// User Id
    s.eVar48=s.getTimeParting('d',gmt).toLowerCase();										// Set day (Jueves)
    s.eVar60=s.getDaysSinceLastVisit('s_lv').toLowerCase();							// Days Since Last Visit
    s.eVar66=s.getTimeParting('w', gmt).toLowerCase();										// Set weekday (laborable/festivo)
    s.eVar70='video';																										// Type player (audio,video)
    s.eVar74="0";																												// Duration
    s.tl(this,'o',s.events);
  }else
  if(event.data == YT.PlayerState.ENDED){
    s.usePlugins = false;
    s=s_gi(s.accountF)
    s.linkTrackVars='events,eVar2,eVar3,eVar4,eVar5,eVar8,eVar17,eVar18,eVar19,eVar20,eVar21,eVar22,eVar30,eVar32,eVar37,eVar39,eVar42,eVar43,eVar48,eVar60,eVar66,eVar70,eVar74';
    s.linkTrackEvents='event11,event12,event13,event14,event79';
    s.events="event12";
    s.eVar2='youtube';																											// player
    s.eVar3=document.domain.replace(/www./gi,"") + location.pathname;				// pageName
    s.eVar4=section.toLowerCase();																					// Section
    if(subsection != '' ){s.eVar5 = section + '>' + subsection;}						// subsection
    s.eVar8=videoData['title'].toLowerCase();																// Name Video
    s.eVar17='web';																													// Canal
    s.eVar18='prisa';																												// Organization
    s.eVar19='radioacktiva';																								// Product
    s.eVar20=document.domain.replace(/www./gi,"");													// Domain|Subdomain
    s.eVar21=s.getNewRepeat().toLowerCase();																// User New / Repeat
    s.eVar22=status;																												// Logueado / Anonymous
    s.eVar30='radio';   													  												// Business Unit
    s.eVar32=s.getVisitNum();																								// Visit Number By Month
    s.eVar39=document.title.toLowerCase();																	// Title
    s.eVar42='vod';														     													// Reproduction (vod,streaming)
    s.eVar43=userId; 																												// User Id
    s.eVar48=s.getTimeParting('d',gmt).toLowerCase(); 											// Set day (Jueves)
    s.eVar60=s.getDaysSinceLastVisit('s_lv').toLowerCase();									// Days Since Last Visit
    s.eVar66=s.getTimeParting('w', gmt).toLowerCase();											// Set weekday (laborable/festivo)
    s.eVar70='video';																												// Type player (audio,video)
    s.eVar74=current_time;																								  // Duration
    s.tl(this,'o',s.events);
  }
}
