var DELAY = 1;
var PROVIDER = "test_AB PRIBERAM";


document.addEventListener("pba-recommendation-ready", function(evt){
  console.log("test_AB ::: el objeto de Priberam existe y llamamos a la función del siguiente "+evt);

  var divContenedor= document.getElementsByClassName("sin-comentarios")[0].getElementsByTagName("li")[0];
  var ancla= divContenedor.getElementsByTagName("a")[0].attributes;
  var titulo= ancla['title'].value; // ancla[2].value;
  var idrefDelProximo= ancla['data-player-idmedia'].value; // ancla[5].value;
  var spanInfo= divContenedor.getElementsByClassName("horario")[0];
  var descripcion= spanInfo.innerText;
  var tiempoDelProximo= descripcion.split("|")[1];//expresado en minutos, hay que pasarlo a segundos

  console.log("test_AB:::idrefDelProximo:::"+idrefDelProximo);
  console.log("test_AB:::titulo:::"+titulo);
  console.log("test_AB:::descripcion:::"+descripcion);

  recursiveExecuteDefinedFunctionByName("PLAYER_SER.player.getMediaPlayer.getUIModule.externalOrder", window, "siguiente",
    {
      "idref":idrefDelProximo,
      "name":titulo,
      "length":"",
      "description":descripcion
    }
  )
})
// var last_path = document.location.pathname.match(/\/[^\/]+\/?$/ig);
// var audio_id = last_path[0].replace(/\//ig, "");
// console.log("[audio_id]" + audio_id);


function create_recommendations() {
  console.log(arguments.callee.name.toString());
  console.log(PROVIDER);

  var el = document.querySelector(".sin-comentarios");
  el.id = "rec_id";

  /*
    if(typeof PLAYER_SER !="undefined"){
      if(typeof PLAYER_SER.player !="undefined"){
        if(typeof PLAYER_SER.player.getMediaPlayer() !="undefined"){
          if(typeof PLAYER_SER.player.getMediaPlayer().getUIModule() !="undefined"){
            if(typeof PLAYER_SER.player.getMediaPlayer().getUIModule().externalOrder !="undefined"){
              PLAYER_SER.player.getMediaPlayer().getUIModule().externalOrder("siguiente",
                {
                  "idref":idrefDelProximo,
                  "name":titulo,
                  "length":"",
                  "description":tiempoDelProximo
                });
            }
          }
        }
      }
    }
  */
  // recursiveExecuteDefinedFunctionByName("PLAYER_SER.player.getMediaPlayer.getUIModule.externalOrder", window, "siguiente",
  //   {
  //     "idref":idrefDelProximo,
  //     "name":titulo,
  //     "length":"",
  //     "description":tiempoDelProximo
  //   }
  // )
}


window.setTimeout(function() {
  //console.log("create_recommendations [DELAY]:::" + DELAY);
  create_recommendations();
}, DELAY);

function recursiveExecuteDefinedFunctionByName(functionName, context /*, args*/) {
  var m_name = "test_AB";  // arguments.callee.name.toString();
  console.log(m_name + ":::");
  console.log(m_name + ":::functionName:::" + functionName.toString());

  var args = Array.prototype.slice.call(arguments, 2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();

  var ns = namespaces.splice(0,1)[0];

  if (typeof ns === 'undefined') {  // Ya estamos en el namespace definitivo porque no quedan elementos en el array namespaces
    console.log(m_name + ":::EJECUTANDO FUNCION:::[" + func + "]");
    args.forEach(function (v, i, a) {
      console.log(m_name + ":::arguments[" + i + "]:::" + JSON.stringify(v));
    });

    return context[func].apply(context, args);  // Ejecutamos la función por su nombre
  } else {  // Todavía quedan niveles para añadir al contexto
    if (typeof context[ns] === 'undefined') {  // Aun no está definida
      console.log(m_name + ":::[" + ns + "] aún no está definida");

      setTimeout(function() {
        recursiveExecuteDefinedFunctionByName.apply(this, [functionName, context].concat(args));
      }, 1000);
      return;
    } else {  // Como el namespace está definido lo añado al contexto
      if (typeof context[ns] == 'function') {
        console.log(m_name + ":::Añade la función [" + ns + "()] al contexto");
        context = context[ns]();
      } else if (typeof context[ns] == 'object') {
        console.log(m_name + ":::Añade el objeto [" + ns + "] al contexto");
        context = context[ns];
      } else {
        console.log(m_name + ":::[" + ns + "] aún no está definida");

        setTimeout(function() {
          recursiveExecuteDefinedFunctionByName.apply(this, [functionName, context].concat(args));
        }, 1000);
        return;
      }
      // Siguiente iteración
      // Re-incorporamos el nombre de la función
      namespaces.push(func)
      var fn = namespaces.join(".");

      recursiveExecuteDefinedFunctionByName.apply(this, [fn, context].concat(args));
    }
  }
}